<?php

/**
 * @file
 * Inspired by http://www.drupalcoder.com/story/406-mixing-private-and-public-downloads-in-drupal-6
 */

/**
 * Implementation of hook_perm().
 */
function private_download_perm() {
  return array('access private download directory');
}

/**
 * Implementation of hook_menu().
 */
function private_download_menu() {
  $items['admin/settings/private_download'] = array(
    'title' => 'Private Download',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('private_download_admin_form'),
    'access arguments' => array('administer site configuration'),
    'description' => 'Manage private download configuration.',
    'file' => 'private_download.admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_file_download().
 */
function private_download_file_download($filepath) {
  // We only care about files inside the private download directory.
  $prefix = variable_get('private_download_directory', 'private') .'/';
  if (strpos($filepath, $prefix) === 0) {

    // Deny access?
    $access = module_invoke_all('private_download_access', substr($filepath, strlen($prefix)));
    $access[] = user_access('access private download directory');
    $allow = in_array(TRUE, $access, TRUE);
    $deny = in_array(FALSE, $access, TRUE);
    $priority = variable_get('private_download_allowdeny_priority', 'deny');
    if ($priority == 'allow' && !$allow || $priority == 'deny' && $deny) {
      return -1;
    }

    // Check whether to serve file as attachment or inline.
    $type = file_get_mimetype($filepath);
    $disposition_attachment = TRUE;
    if (variable_get('private_download_disposition', 'attachment') == 'inline') {
      $inline_patterns = variable_get('private_download_inline_patterns', array('^text/', '^image/', '\bflash$', '\bpdf$'));
      foreach ($inline_patterns as $pattern) {
        if (preg_match('`'. $pattern .'`', $type)) {
          $disposition_attachment = FALSE;
          break;
        }
      }
    }

    // Define default file header attributes.
    // We only send the "Content-Disposition" header field for disposition-type
    // "attachment". Otherwise, we unset it rather than sending a
    // disposition-type "inline", to minimize potential problems.
    $header = array(
      'Content-Type: '. $type,
      'Content-Length: '. filesize(file_create_path($filepath)),
      'Content-Disposition: '. ($disposition_attachment ? 'attachment; filename="'. mime_header_encode(basename($filepath)) .'"' : ''),
    );

    // Add user-defined file header attributes, if any.
    return array_merge($header, explode("\n", variable_get('private_download_header', "Content-Transfer-Encoding: binary\nCache-Control: private")));
  }
}

/**
 * Write data to a file.
 * 
 * @param string
 * @param string
 * @return boolean
 */
function private_download_write($filename, $content) {
  // Write content to file; create file if not present.
  $success = FALSE;
  if ($handle = @fopen($filename, 'w+b')) {
    $success = (fwrite($handle, $content) !== FALSE);
    fclose($handle);
  }
  return $success;
}

/**
 * Read data from a file.
 * 
 * @param string
 * @return string or boolean
 */
function private_download_read($filename) {
  // Get file content into a string.
  $content = FALSE;
  if ($handle = @fopen($filename, 'r')) {
    $content = fread($handle, filesize($filename));
    fclose($handle);
  }
  return $content;
}